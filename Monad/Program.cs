﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using Sprache;

namespace Monad
{
    class Program
    {
        static void Main(string[] args)
        {
         


            if (args.Length == 0 || !File.Exists(args[0]))
            {
                Console.WriteLine("Файл не найден");
                return;
            }

            try
            {
                string input = File.ReadAllText(args[0]);
                var res = S.Parse(input);

                var enumerable = res as IList<Command> ?? res.ToList();

                Console.WriteLine("Операции:");
                Output(enumerable);

                Console.WriteLine("Дерево вывода:");
                Console.WriteLine("S->");
                OutputTree(enumerable, 1);
            }
            catch (Exception exception)
            {
                Console.WriteLine("ERROR");
                Console.WriteLine(exception.Message);
            }
            
        }
        private static void OutputTree(IEnumerable<Command> commands, int deep = 1)
        {
            foreach (var command in commands)
            {
                SpacesToConsole(deep);
                Console.Write(CommandToStringForTree(command, deep) + "\n\n");
                if (command.Operations != null && command.Operations.Any())
                    OutputTree(command.Operations, deep + 2);
            }
        }

        public static string GetVarOutput(string text)
        {
            try
            {
                Int32.Parse(text);
                return "Var->Number->" + text;
            }
            catch { }

            return "Var->" + text;
        }

        private static string CommandToStringForTree(Command command, int deep)
        {
            switch (command.Type)
            {
                case Command.CommandType.Assigment:
                    string output = "Assigment->\t" + GetVarOutput(command.Operand1)
                        + "\n" + Spaces(deep + 2) + GetVarOutput(command.Operand2);
                    return output;
                case Command.CommandType.NewLine:
                    return "NewLine->eps";
                case Command.CommandType.Split:
                    return "Split->eps";
                case Command.CommandType.Skip:
                    return "Skip->eps";
                case Command.CommandType.Read:
                    return "Read->" + GetVarOutput(command.Operand1);
                case Command.CommandType.Write:
                    return "Write->" + GetVarOutput(command.Operand1);
                case Command.CommandType.If:
                    {
                        string condition = string.Empty;
                        if (!string.IsNullOrWhiteSpace(command.Condition.Operation))
                            condition = "Expression->\t" + GetVarOutput(command.Condition.Op1)
                                + "\n" + Spaces(deep + 4) + "Operation-> " + (command.Condition.Operation) +
                                         "\n" + Spaces(deep + 4) + GetVarOutput(command.Condition.Op2);
                        else
                            condition = "Expression->" + GetVarOutput(command.Condition.Op1);

                        return "IfCondition->   " + condition;
                    }
                case Command.CommandType.While:
                    {
                        string condition = string.Empty;
                        if (!string.IsNullOrWhiteSpace(command.Condition.Operation))
                            condition = "Expression->\t" + GetVarOutput(command.Condition.Op1)
                                + "\n" + Spaces(deep + 4) + "Operation-> " + command.Condition.Operation +
                                "\n" + Spaces(deep + 4) + GetVarOutput(command.Condition.Op2);
                        else
                            condition = "Expression->" + GetVarOutput(command.Condition.Op1);

                        return "WhileCondition->   " + condition ;
                    }


            }
            return string.Empty;
        }


        private static string CommandToString(Command command)
        {
            switch (command.Type)
            {
                case Command.CommandType.Assigment:
                    return "Assigment(" + command.Operand1 + "," + command.Operand2 + ")";
                case Command.CommandType.NewLine:
                    return "NewLine()";
                case Command.CommandType.Split:
                    return "Split()";
                case Command.CommandType.Skip:
                    return "Skip()";
                case Command.CommandType.Read:
                    return "Read(" + command.Operand1 + ")";
                case Command.CommandType.Write:
                    return "Write(" + command.Operand1 + ")";
                case Command.CommandType.If:
                    {
                        string condition = string.Empty;
                        if (!string.IsNullOrWhiteSpace(command.Condition.Operation))
                            condition = "Expression(" + command.Condition.Op1 + "," + command.Condition.Operation + "," +
                                               command.Condition.Op2 + ")";
                        else
                            condition = "Expression(" + command.Condition.Op1 + ")";

                        return "IfCondition(" + condition + ")";
                    }
                case Command.CommandType.While:
                    {
                        string condition = string.Empty;
                        if (!string.IsNullOrWhiteSpace(command.Condition.Operation))
                            condition = "Expression(" + command.Condition.Op1 + "," + command.Condition.Operation + "," +
                                               command.Condition.Op2 + ")";
                        else
                            condition = "Expression(" + command.Condition.Op1 + ")";

                        return "WhileCondition(" + condition + ")";
                    }


            }
            return string.Empty;
        }

        private static void SpacesToConsole(int deep) => Enumerable.Range(0, deep).ToList().ForEach(x => Console.Write("\t"));

        private static string Spaces(int deep)
        {
            string output = String.Empty;
            Enumerable.Range(0, deep).ToList().ForEach(x => output += "\t");
            return output;
        }

        private static void Output(IEnumerable<Command> commands, int deep = 0)
        {
            foreach (var command in commands)
            {
                SpacesToConsole(deep);
                Console.Write(CommandToString(command) + "\n");
                if (command.Operations != null && command.Operations.Any())
                    Output(command.Operations, deep + 1);
            }
        }


        private static bool VariableIsValid(string text)
        {

            var op = text.Trim();
            if (!string.IsNullOrEmpty(op) && KeyWords.Contains(op))
                throw new ParseException("Неверное название переменной");

            return true;
        }

        private static Parser<string> Variable =
              (from letterPartVar in Parse.Letter.AtLeastOnce().Text()
               from digitPartVar in Parse.Digit.Many().Text()
               let result = letterPartVar + digitPartVar
               where VariableIsValid(result)
               select result).Text();

        private static Parser<ExpressionType> VariableAsExpression = Variable.Select(x => new ExpressionType() { Op1 = x });
        private static Parser<ExpressionType> VariableAsExpressionInParentheses = from lparen in Parse.Char('(')
                                                                                  from expr in (VariableAsExpression).Token()
                                                                                  from rparen in Parse.Char(')')
                                                                                  select expr;


        private static Parser<ExpressionType> VariableCommon
            => VariableAsExpressionInParentheses.XOr(VariableAsExpression);

        private static string[] operationsStrings = new[]
        {
            ">",
            "<",
            "<=",
            ">=",
            "+",
            "-",
            "*",
            "/",
            "==",
            "!=",
            "&&",
            "||",
        };

        private static string[] KeyWords = new[] { "read", "if", "then", "skip", "do" };
        private static char[] operationChars = new[] { '>', '<', '=', '+', '*' };

        private static bool OperationIsValid(string operation)
        {
            var op = operation.Trim();
            if (!string.IsNullOrEmpty(op) && !operationsStrings.Contains(operation))
                throw new Exception("Операции не существует");
            return true;
        }

        private static Parser<string> Operations =
            from op in Parse.Chars(operationChars).AtLeastOnce().Text()
            where OperationIsValid(op)
            select op;

        public class ExpressionType
        {
            public string Op1 { get; set; }
            public string Op2 { get; set; }
            public string Operation { get; set; }
        }
        public class WhileType
        {
            public ExpressionType Exp { get; set; }
            public string Operation { get; set; }
        }




        private static Parser<string> Empty = from o in Parse.Return(Empty) select String.Empty;


        private static Parser<ExpressionType> Expression
            = from operand1 in VariableAsExpression.Token()
              from operation in Operations.Or(Empty).Token()
              from operand2 in !string.IsNullOrEmpty(operation) ? Variable.XOr(NumbersAsString) : Empty
              select new ExpressionType() { Operation = operation, Op1 = operand1.Op1, Op2 = operand2 };


        private static Parser<ExpressionType> Numbers = from num in Parse.Digit.Many().Text().Token() select new ExpressionType() { Op1 = num, };
        private static Parser<string> NumbersAsString = from num in Parse.Digit.Token() select num.ToString();
        private static Parser<ExpressionType> ExpressionCommon => ExpressionInParentheses.XOr(Expression).XOr(Numbers);

        private static Parser<ExpressionType> ExpressionInParentheses = from lparen in Parse.Char('(')
                                                                        from expr in (Expression).Token()
                                                                        from rparen in Parse.Char(')')
                                                                        select expr;


        private static Parser<Command> IfConfition = (from ifBegin in Parse.String("if").Token().Text()
                                                      from exp in ExpressionCommon.Token()
                                                      from thenCond in Parse.String("then").Token().Text()
                                                      from operations in CommandExp.Token()
                                                      from fiCond in Parse.String("fi").Token().Text()
                                                          /*   select new Command()
                                                             {
                                                                 Condition = exp,
                                                                 Operations = operations.Select(x => new Command()
                                                                 {
                                                                     Type = x.Type,
                                                                     Condition = x.Condition,
                                                                     Operations = x.Operations,
                                                                     Operand = x.Operand
                                                                 }),
                                                                 Type = Command.CommandType.If
                                                             });*/
                                                      select new Command() { Condition = exp, Operand1 = operations.Operand1, Operations = new List<Command>() { operations }, Operand2 = operations.Operand2, Type = Command.CommandType.If });

        private static Parser<Command> WhileLoop = (from whileLoop in Parse.String("while").Token().Text()
                                                    from exp in ExpressionCommon.XOr(VariableCommon).Token()
                                                    from doLoop in Parse.String("do").Token().Text()
                                                    from operations in Seq.Token()
                                                    from odLoop in Parse.String("od").Token().Text()
                                                        /*   select new Command()
                                                           {
                                                               Condition = exp,
                                                               Operations = operations.Select(x => new Command()
                                                               {
                                                                   Type = x.Type,
                                                                   Condition = x.Condition,
                                                                   Operations = x.Operations,
                                                                   Operand1 = x.Operand1,
                                                                   Operand2 = x.Operand2
                                                               }),
                                                               Type = Command.CommandType.While
                                                           });*/
                                                    select new Command() { Condition = exp, Operand1 = operations.Operand1, Operations = new List<Command>() { operations }, Operand2 = operations.Operand2, Type = Command.CommandType.While });


        private static Parser<Command> SplitCommand = from x in Parse.Char(';').AtLeastOnce().Token()
                                                      select new Command() { Type = Command.CommandType.Split, Operand1 = ";" };

        private static Parser<Command> NewLineCommand = from x in Parse.Char('\n').AtLeastOnce().Token()
                                                        select new Command() { Type = Command.CommandType.NewLine };

        private static Parser<Command> SkipCommand = from x in Parse.String("skip").Token().Text()
                                                     select new Command() { Type = Command.CommandType.Skip };
        private static Parser<Command> ReadCommand = from x in Parse.String("read").Token().Text()
                                                     from operand in VariableCommon.Token()
                                                     select new Command() { Type = Command.CommandType.Read, Operand1 = operand.Op1 };
        private static Parser<Command> WriteCommand = from x in Parse.String("write").Token().Text()
                                                      from operand in VariableCommon
                                                      select new Command() { Type = Command.CommandType.Write, Operand1 = operand.Op1 };

        private static Parser<Command> AssigmentCommand = from var in Variable
                                                          from x in Parse.String(":=").Token().Text()
                                                          from operand in ExpressionCommon
                                                          select new Command() { Type = Command.CommandType.Assigment, Operand2 = operand.Op1, Operand1 = var };


        private static Parser<Command> Seq = IfConfition.XOr(WhileLoop).XOr(ReadCommand).XOr(SkipCommand).XOr(WriteCommand).XOr(AssigmentCommand).XOr(NewLineCommand).XOr(SplitCommand);

        private static Parser<Command> CommandExp = ReadCommand.XOr(WriteCommand).XOr(AssigmentCommand).XOr(SplitCommand);

        private static Parser<IEnumerable<Command>> S
            => Seq.Select(x => new Command() { Type = x.Type, Operand1 = x.Operand1, Operand2 = x.Operand2, Condition = x.Condition, Operations = x.Operations }).Repeat(0, 128);

        private class Command
        {
            public enum CommandType
            {
                None,
                Read,
                Write,
                Skip,
                Split,
                Assigment,
                If,
                While,
                NewLine
            }
            public ExpressionType Condition { get; set; }
            public CommandType Type { get; set; }
            public IEnumerable<Command> Operations;
            public string Operand1 { get; set; }
            public string Operand2 { get; set; }
        }

    }
}
