Консольное win-приложение. Собиралось под net framework 4.0. Написано на C# с использованием библиотеки Sprache (https://github.com/sprache/Sprache)


В репозитории находится папка DZ в которой лежит бинарник Monad.exe с тестами.

Все исходники прилагаются. 

Запуск:
1. Вызов командной строки CMD
2. Переход в директорию cd path/DZ
3. Вызов программы "Monad Test1.txt"

 